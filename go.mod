module gitlab.com/NateWilliams2/vocexplorer

go 1.14

require (
	github.com/gopherjs/vecty v0.0.0-20200328200803-52636d1f7aba
	github.com/nathanhack/vectyUI v0.0.0-20200328214448-958e9d362de4
	gitlab.com/vocdoni/go-dvote v0.4.0
	marwan.io/vecty-router v0.0.0-20190701140924-0b5c86ca8801
)
